package com.android.noochd.redditreader;

import android.net.Uri;

/**
 * Created by Student on 10/8/2015.
 */
public interface ActivityCallback {

    void onPostSelected(Uri redditPostUri);
}
