package com.android.noochd.redditreader;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RedditPostTask extends AsyncTask<RedditListFragment, Void, JSONObject> {
    private RedditListFragment redditListFragment;

    @Override
    protected JSONObject doInBackground(RedditListFragment... redditListFragments) {
        JSONObject jsonObject = null;
        redditListFragment = redditListFragments[0];
        try {
            URL redditFeedUrl = new URL("https://Reddit.com/r/android.json");

            HttpURLConnection httpConnection = (HttpURLConnection)redditFeedUrl.openConnection();
            httpConnection.connect();

            int statusCode = httpConnection.getResponseCode();

            if(statusCode == HttpsURLConnection.HTTP_OK){
                //parse through input stream
               jsonObject = RedditPostParser.getInstance().parseInputStream(httpConnection.getInputStream());

            }
        }
        catch(MalformedURLException error){
            Log.e("RedditPostTask", "MalformedURLExpection (doInBackground):  " + error);
        }
        catch (IOException error){
            Log.e("RedditPostTask ", "IOExpection (doInBackground): " + error);


        }
        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        RedditPostParser.getInstance().readRedditFeed(jsonObject);
        redditListFragment.updateUserInterface();

    }
}
